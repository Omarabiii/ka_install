import os
import sys
import time
import hashlib
import random
import json
SIGN="update230401"
UPDATE_DATE="2023.4.1"

print("Loading...")
version_get=os.popen(str("curl https://res.hestudio.org/kali_for_android/"+SIGN+".txt"))
ads_get=os.popen(str("curl https://res.hestudio.org/res/ads.json"))
ads = json.load(ads_get)
time.sleep(5)
if not "True" in version_get.read():
    print("\r此版本过于老旧，请删除ka_install文件夹和install.sh脚本，然后再按照教程重新开始安装！")
    print("\n附教程链接：https://www.hestudio.org/posts/install-kali-on-android-renew.html")
    sys.exit()

def output_info():
    os.system("clear")
    print("""
kali install for Termux 
    
作者：醉、倾城@heStudio
    
联系邮箱：hestudio@hestudio.org
QQ频道：https://pd.qq.com/s/uakgta
作者CSDN：醉、倾城
heStudio社区：https://bbs.csdn.net/forums/hestudio
作者Gitee：https://gitee.com/heStudio
作者Github：https://github.com/heStudio-Network
作者博客：https://www.hestudio.org/
作者爱发电：https://afdian.net/@hestudio
作者Telegram群组：https://t.me/hestudio_network
RSS 订阅：https://www.hestudio.org/atom.xml
问题反馈：https://www.hestudio.org/get-help
heStudio Talking: https://www.hestudio.org/talking
    
[广告位招租]有意者联系作者↑↑↑
    
    """)
    
    for ad in ads["ads"]:
        print("[广告]",ad)
    print("""
如遇到安装错误，可以尝试删除termux目录的一些关于kali的文件，ka_install文件夹，install.sh，再重新开始。否则会报错。

安装之前，请你重新阅读：https://www.hestudio.org/posts/install-kali-on-android-renew.html 
具体的改动以上面的链接为准！

怎么获取key&找到已购买的key: https://www.hestudio.org/docs/key.html

如果遇到问题，请先浏览 https://www.hestudio.org/docs/kali_for_android.html
如果遇到的问题文档里没有，请到 https://www.hestudio.org/get-help 反馈。
如果反馈的问题在5个小时没有回复（阴间时间除外），请进QQ频道。
QQ频道： https://pd.qq.com/s/uakgta
   
赞助的所有资金用于更好的提供服务。毕竟都不想用着卡顿的服务器下载文件。
    """)
    
    print("最近key更新时间", UPDATE_DATE)
    print("版本代号:", SIGN)
    print("输入key后将开始安装进程。")

output_info()
key_input = input("\r输入获取的key：")
print("\n\nLoading...")
key_hash = hashlib.sha256(key_input.encode('utf-8')).hexdigest()
check=os.popen(str("curl https://res.hestudio.org/kali_for_android/"+key_hash+"/check.txt"))
time.sleep(2)
if "True" in check.read():
    os.system(str("wget https://res.hestudio.org/kali_for_android/"+key_hash+"/install.zip"))
    time.sleep(2)
    os.system("unzip install.zip")
    os.system("rm install.zip")
    os.system("chmod +x .install/kalinethunter")
    os.system("chmod +x .install/finaltouchup.sh")
    os.system("./.install/kalinethunter")
    os.system("startkali")
else:
    os.system("clear")
    output_info()
    print("\r输入获取的key："+key_input)
    print("\nkey错误，请重新执行教程中的命令再次运行本工具")
    sys.exit()

