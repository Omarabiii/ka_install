# kali install for Termux 

## 本次更新
开源部分组件

## 作者

### [醉、倾城](https://www.hestudio.org/about)

- 联系邮箱：hestudio@hestudio.org
- QQ频道：https://pd.qq.com/s/uakgta
- 作者CSDN：醉、倾城
- heStudio社区：https://bbs.csdn.net/forums/hestudio
- 作者Gitee：https://gitee.com/heStudio
- 作者Github：https://github.com/heStudio-Network
- 作者博客：https://www.hestudio.org/
- 作者爱发电：https://afdian.net/@hestudio
- 作者Telegram群组：https://t.me/hestudio_network
- RSS 订阅：https://www.hestudio.org/atom.xml
- 问题反馈：https://www.hestudio.org/get-help
- heStudio Talking: https://www.hestudio.org/talking

## 最近更新日期 & 更新代号
* 最近更新日期：2023.4.1
* 更新代号：update230401

## 传送门
### 教程
https://www.hestudio.org/posts/install-kali-on-android-renew.html

### 文档
https://www.hestudio.org/docs/kali_for_android.html

### 怎么获取key&找到已购买的key
https://www.hestudio.org/docs/key.html
 
### 官方文档
https://www.kali.org/docs/nethunter/nethunter-rootless/


# 赞助(赞助资金全部用来续费服务器)
[![赞助资金全部用来续费服务器](https://res.hestudio.org/res/afdian.jpg)](https://afdian.net/@hestudio)
